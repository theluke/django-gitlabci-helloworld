from django.test import TestCase


class HelloWorldTestCase(TestCase):
    def test_get_returns_200_OK(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
